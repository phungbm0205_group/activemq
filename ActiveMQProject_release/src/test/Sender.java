package test;
import java.util.Properties;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.apache.log4j.BasicConfigurator;
import entity.*;
import helper.*;

public class Sender {
	public static void main(String[] args) throws Exception{
		//khoi tao connection
		//start
		BasicConfigurator.configure();
		Properties settings = new Properties();
		settings.setProperty(Context.INITIAL_CONTEXT_FACTORY,"org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		settings.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");
		Context ctx=new InitialContext(settings);
		ConnectionFactory factory = (ConnectionFactory)ctx.lookup("ConnectionFactory");
		Destination destination = (Destination) ctx.lookup("dynamicQueues/BuiMinhPhung");

		Connection con = factory.createConnection("admin","admin");
		con.start();
		//end
		
		//khoi tao 1 session
		Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
		MessageProducer producer = session.createProducer(destination);
		//Khoi tao object send len MQ, cho nay co the thay the bang object khac, 
		SinhVien sv = new SinhVien("test lan 2", 12312312);
		//parse sang json/xml
		try {
			String json =new JsonConverter<SinhVien>(sv).object2Json(sv);
			Message msg = session.createTextMessage(json);
			//send
			producer.send(msg);
			System.out.println("Gửi thành công");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Gửi khong thành công");
			
		}finally {
			session.close();
			con.close();
		}
		
	
		
		
	}
}
