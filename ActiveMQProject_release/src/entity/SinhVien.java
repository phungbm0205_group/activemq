package entity;

public class SinhVien {
	private String name;
	private int mssv;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMssv() {
		return mssv;
	}
	public void setMssv(int mssv) {
		this.mssv = mssv;
	}
	public SinhVien(String name, int mssv) {
		super();
		this.name = name;
		this.mssv = mssv;
	}
	public SinhVien() {
		
		this.name = "";
		this.mssv = 0;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mssv;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass().getSuperclass() != obj.getClass().getSuperclass())
			return false;
		SinhVien other = (SinhVien) obj;
		if (mssv != other.mssv)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SinhVien [name=" + name + ", mssv=" + mssv + "]";
	}
	
	
}
