package helper;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import entity.SinhVien;

public class JsonConverter<T> {
	private T type;
	
	public JsonConverter(T sv) {
		this.type = type;
	}
	
	public String object2Json(T obj)throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		try {
		    String json = mapper.writeValueAsString(obj);
		    return json;
		} catch (IOException e) {
		    e.printStackTrace();
		    return null;
		}
		
	}
	@SuppressWarnings("unchecked")
	public T json2Object(String json) {
		try {
			ObjectMapper mapper = new ObjectMapper();
		    SinhVien sv = mapper.readValue(json, SinhVien.class);
		    return (T) sv;
		} catch (IOException e) {
		    e.printStackTrace();
		    return null;
		}
		
	}
}
